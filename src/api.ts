import axios from 'axios';
import { axiosData } from './interfaces/interface';

export const fetchDataHeroes = async (
  nameHero: string | string[] | null,
  sortBy: string | string[] | null,
  page: string | string[] | null,
) => {
  const URL = 'https://gateway.marvel.com:443/v1/public/characters';
  // parseInt(page);
  try {
    const { data } = await axios.get<axiosData>(`${URL}`, {
      params: {
        apikey: '01207367c92293265d9c513ac2c3ffca',
        limit: page ? 5 * +page : 5,
        orderBy: sortBy,
        nameStartsWith: nameHero,
      },
    });
    console.log('конец запроса на сервер');
    return data.data;
  } catch (err) {
    console.log('Ошибка', err);
    throw new Error('Error');
  }
};

export const fetchDataComics = async (id: number, page: string | string[] | null) => {
  try {
    const URL = `https://gateway.marvel.com:443/v1/public/characters/${id}/comics`;
    const { data } = await axios.get<axiosData>(`${URL}`, {
      params: {
        apikey: '01207367c92293265d9c513ac2c3ffca',
        limit: page ? 2 * +page : 2,
      },
    });
    return data.data;
  } catch (err) {
    console.log('Ошибка', err);
    throw new Error('Error');
  }
};
