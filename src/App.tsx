import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import HomePage from "./components/home/HomePage";
import ComicsPage from "./components/comics/ComicsPage";
import NotFoundPage from "./components/notFoundPage/NotFoundPage";
import "./App.css";

const App = (): JSX.Element => {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path={["/", "/search"]} component={HomePage} exact />
          <Route path="/heroes/:id" component={ComicsPage} />
          <Route component={NotFoundPage} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
