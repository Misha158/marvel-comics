import React from 'react';

const NotFoundPage: React.FC<Record<string, never>> = () => {
  return (
    <>
      <h1>NOTFOUND PAGE</h1>
    </>
  );
};

export default NotFoundPage;
