import React from 'react';
import { ComicsResultsProps } from '../../interfaces/interface';
import Spinner, { Size } from '@atlaskit/spinner';
import ComicsCard from '../comicsCard/ComicsCard';

const sizes: Size = 'xlarge';

const ComicsResults: React.FC<ComicsResultsProps> = ({ comics, loading }: ComicsResultsProps) => {
  const comicsLIst = comics!.map((comic) => {
    return (
      <ComicsCard
        key={comic.id}
        id={comic.id}
        thumbnail={comic.thumbnail.path}
        extension={comic.thumbnail.extension}
        description={comic.description}
        name={comic.name}
      />
    );
  });
  const check = comicsLIst.length !== 0 ? comicsLIst : <div className="hero__nofound">Sorry! Not found comics</div>;
  return <div className="comics__card_container">{loading ? check : <Spinner size={sizes} />}</div>;
};

export default ComicsResults;
