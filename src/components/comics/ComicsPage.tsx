import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import queryString from 'query-string';
import { ComicsState, ComicsPageRouteProps } from '../../interfaces/interface';
import ComicsResults from './ComicsResults';
import { fetchDataComics } from '../../api';
import ContainerComics from '../../shared/ContainerComics';
import Pagination from '../pagination/Pagination';
import Title from '../../shared/Title';
import ButtonGoBack from '../../shared/ButtonGoBack';

class ComicsPage extends React.Component<RouteComponentProps<ComicsPageRouteProps>> {
  state: Readonly<ComicsState> = {
    comics: [],
    loading: false,
    total: 0,
  };

  componentDidMount() {
    this.fetchComics();
  }

  componentDidUpdate(prevProps: RouteComponentProps) {
    if (prevProps !== this.props) {
      this.fetchComics();
    }
  }

  fetchComics = () => {
    const params = queryString.parse(this.props.location.search);
    const { match } = this.props;
    const { id } = match.params;

    fetchDataComics(parseInt(id), params.page).then((data) =>
      this.setState({
        comics: data.results,
        total: data.total,
        loading: true,
      }),
    );
  };

  pagination = () => {
    const params = queryString.parse(this.props.location.search);
    const newParams = queryString.stringify({
      ...params,
      page: typeof params.page === 'string' ? parseInt(params.page) + 1 : 2,
    });
    this.props.history.push(`?${newParams}`);
  };

  render() {
    console.log(this.state);
    const { comics, loading, total } = this.state;
    const check = total > comics.length ? <Pagination pagination={this.pagination} /> : '';
    return (
      <ContainerComics>
        <ButtonGoBack {...this.props} />
        <Title class={'title title__comics'} text={`Awesome Comics`} />
        <ComicsResults comics={comics} loading={loading} />
        {loading ? check : ''}
      </ContainerComics>
    );
  }
}

export default ComicsPage;
