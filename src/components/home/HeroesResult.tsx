import React from 'react';
import Spinner, { Size } from '@atlaskit/spinner';
import HeroesCard from '../heroesCard/HeroesCard';
import { HeroesResultProps } from '../../interfaces/interface';

const sizes: Size = 'xlarge';

const HeroesResult: React.FC<HeroesResultProps> = ({ heroes, loading }: HeroesResultProps) => {
  const heroesList = heroes!.map((hero) => {
    return (
      <HeroesCard
        key={hero.id}
        id={hero.id}
        thumbnail={hero.thumbnail.path}
        extension={hero.thumbnail.extension}
        name={hero.name}
      />
    );
  });
  const check =
    heroesList.length !== 0 ? (
      heroesList
    ) : (
      <div className="hero__nofound">
        Sorry! This hero doesn&apos;t exist <br /> Try to find another hero
      </div>
    );
  return <div>{loading ? check : <Spinner size={sizes} />}</div>;
};

export default HeroesResult;
