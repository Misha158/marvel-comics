import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import queryString from 'query-string';
import HeroesResult from './HeroesResult';
import Title from '../../shared/Title';
import Logo from '../../shared/Logo';
import { HomePageProps, HomePageState } from '../../interfaces/interface';
import { fetchDataHeroes } from '../../api';
import SearchPanel from '../searchPanel/SearchPanel';
import Container from '../../shared/Container';
import Pagination from '../pagination/Pagination';
import '../../style/style.css';

class HomePage extends React.Component<RouteComponentProps<HomePageProps>, HomePageState> {
  state: Readonly<HomePageState> = {
    loading: false,
    inputValue: '',
    heroes: [],
    total: 0,
  };

  componentDidMount() {
    console.log('componentDidMount');
    this.fetchHeroes();
  }

  componentDidUpdate(prevProps: RouteComponentProps) {
    if (prevProps !== this.props) {
      console.log('componentDidUpdate');
      console.log(this.state);
      this.fetchHeroes();
      const params = queryString.parse(this.props.location.search);

      this.setState({
        inputValue: '',
      });
    }
  }

  fetchHeroes = () => {
    const params = queryString.parse(this.props.location.search);
    fetchDataHeroes(params.query, params.orderBy, params.page).then((data) =>
      this.setState({
        heroes: data.results,
        total: data.total,
        loading: true,
      }),
    );
  };

  handlerSubmit = (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();
    if (this.state.inputValue !== '') {
      this.props.history.push(`/search?query=${this.state.inputValue}`);
      this.setState({
        loading: false,
      });
    }
  };

  changeHeroName = (value: any) => {
    this.setState({ inputValue: value });
  };

  pagination = () => {
    const params = queryString.parse(this.props.location.search);
    const newParams = queryString.stringify({
      ...params,
      page: typeof params.page === 'string' ? parseInt(params.page) + 1 : 2,
    });
    this.props.history.push(`?${newParams}`);
  };

  handlerSortBy = (btn: string) => {
    const params = queryString.parse(this.props.location.search);
    const newParams = queryString.stringify({
      ...params,
      orderBy: btn,
    });
    this.props.history.push(`/?${newParams}`);
  };

  render() {
    const params = queryString.parse(this.props.location.search);
    const { heroes, loading, total } = this.state;
    const check = total > heroes.length ? <Pagination pagination={this.pagination} /> : '';
    console.log(this.state);
    return (
      <>
        <Logo />
        <Container>
          <Title class={'title'} text={'Find your hero'} />
          <SearchPanel
            onSubmit={this.handlerSubmit}
            changeHeroName={this.changeHeroName}
            sort={this.handlerSortBy}
            inputValue={this.state.inputValue}
          />
          <HeroesResult heroes={heroes} loading={loading} />
          {loading ? check : ''}
        </Container>
      </>
    );
  }
}

export default HomePage;
