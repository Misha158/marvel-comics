import React from 'react';
import { Link } from 'react-router-dom';
import cat from '../../images/cat.jpg';
import Button from '@atlaskit/button';

interface HeroesCardProps {
  id: number;
  thumbnail: string;
  name: string;
  extension: string;
}

const HeroesCard: React.FC<HeroesCardProps> = (props: HeroesCardProps) => {
  return (
    <Link to={`/heroes/${props.id}`} key={props.id}>
      <div className="hero__card">
        <img
          className="hero__photo"
          src={
            `${props.thumbnail}.${props.extension}` !==
            'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg'
              ? `${props.thumbnail}/standard_fantastic.jpg`
              : `${cat}`
          }
          alt={`${props.name}`}
        />
        <h2 className="hero__name">{props.name}</h2>
        <Button appearance="danger" className="hero__btn btn">
          detail
        </Button>
      </div>
    </Link>
  );
};

export default HeroesCard;
