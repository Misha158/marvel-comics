import React from 'react';
import Button from '@atlaskit/button';

interface SearchButtonProps {
  onSubmit: (e: React.FormEvent<EventTarget>) => void;
}

const SearchButton: React.FC<SearchButtonProps> = (props: SearchButtonProps) => {
  return (
    <Button
      appearance="danger"
      className="search__btn btn"
      onClick={(e) => {
        e.preventDefault();
        props.onSubmit(e);
      }}
    >
      Find heroes
    </Button>
  );
};

export default SearchButton;
