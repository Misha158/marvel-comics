import React from 'react';
import Button from '@atlaskit/button';

interface SortByNameButtonProps {
  sort: (btn: string) => void;
}

const SortByNameButton: React.FC<SortByNameButtonProps> = (props: SortByNameButtonProps) => {
  return (
    <Button
      appearance="danger"
      className="seach__sortby btn"
      onClick={() => {
        props.sort('name');
      }}
    >
      sort by name
    </Button>
  );
};

export default SortByNameButton;
