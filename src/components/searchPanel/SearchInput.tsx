import React from 'react';

interface SearchInputProps {
  changeHeroName: (value: string) => void;
  value: string;
}

const SearchInput: React.FC<SearchInputProps> = (props: SearchInputProps) => {
  return (
    <input
      className="search__input"
      onChange={(e) => {
        const value = e.target.value;
        props.changeHeroName(value);
      }}
      type="text"
      value={props.value}
    />
  );
};

export default SearchInput;
