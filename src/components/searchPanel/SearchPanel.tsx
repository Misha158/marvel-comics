import React from 'react';
import SearchInput from './SearchInput';
import SearchButton from './SearchButton';
import SortByModifiedButton from './SortByModifiedButton';
import SortByNameButton from './SortByNameButton';

interface SearchPanelProps {
  onSubmit: (e: React.FormEvent<EventTarget>) => void;
  changeHeroName: (value: string) => void;
  sort: (btn: string) => void;
  inputValue: string;
}

const SearchPanel: React.FC<SearchPanelProps> = (props: SearchPanelProps) => {
  return (
    <div>
      <form
        className="search__panel"
        onSubmit={(e) => {
          e.preventDefault();
          props.onSubmit(e);
        }}
      >
        <SearchInput changeHeroName={props.changeHeroName} value={props.inputValue} />
        <div className="box__search_btn">
          <SortByModifiedButton sort={props.sort} />
          <SortByNameButton sort={props.sort} />
          <SearchButton onSubmit={props.onSubmit} />
        </div>
      </form>
    </div>
  );
};

export default SearchPanel;
