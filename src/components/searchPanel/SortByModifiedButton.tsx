import React from 'react';
import Button from '@atlaskit/button';

interface SortByModifiedButtonProps {
  sort: (btn: string) => void;
}

const SortByModifiedButton: React.FC<SortByModifiedButtonProps> = (props: SortByModifiedButtonProps) => {
  return (
    <Button
      appearance="danger"
      className="seach__sortby btn"
      onClick={() => {
        props.sort('-modified');
      }}
    >
      sort by modified
    </Button>
  );
};

export default SortByModifiedButton;
