import React from 'react';
import Button from '@atlaskit/button';

interface PaginationProps {
  pagination: () => void;
}

const Pagination: React.FC<PaginationProps> = (props: PaginationProps) => {
  return (
    <Button
      appearance="danger"
      onClick={() => {
        props.pagination();
      }}
      className="heroes__pagination btn"
    >
      More
    </Button>
  );
};

export default Pagination;
