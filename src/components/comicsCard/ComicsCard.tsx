import React from 'react';

interface ComicsCardProps {
  key: number;
  id: number;
  thumbnail: string;
  extension: string;
  name: string;
  description: string;
}

const ComicsCard: React.FC<ComicsCardProps> = (props: ComicsCardProps) => {
  return (
    <div className="comics__card">
      <img className="comics__photo" src={`${props.thumbnail}/portrait_uncanny.jpg`} alt={`${props.name}`} />
      <p className="comics__description">{props.description}</p>
    </div>
  );
};

export default ComicsCard;
