import React from 'react';

interface TitleProps {
  text: string;
  class: string;
}

const Title: React.FC<TitleProps> = (props: TitleProps) => {
  return <h1 className={props.class}>{props.text}</h1>;
};

export default Title;
