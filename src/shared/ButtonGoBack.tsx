import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { HomePageProps } from '../interfaces/interface';
import Button from '@atlaskit/button';

const ButtonGoBack: React.FC<RouteComponentProps<HomePageProps>> = (props: RouteComponentProps) => {
  const handlerGoBack = () => {
    props.history.goBack();
  };

  return (
    <Button appearance="danger" onClick={handlerGoBack} className="btn_goBack btn">
      go back
    </Button>
  );
};

export default ButtonGoBack;
