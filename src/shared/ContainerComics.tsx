import React from 'react';

interface ContainerComicsProps {
  children: React.ReactNode;
}

const ContainerComics: React.FC<ContainerComicsProps> = (props: ContainerComicsProps) => {
  return <div className="container__comics container">{props.children}</div>;
};

export default ContainerComics;
