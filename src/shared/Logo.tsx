import React from 'react';
import logo from '../images/logo.jpg';

const Logo: React.FC = () => {
  return (
    <div className="container__main">
      <div className="logo">
        <div className="linear-gradient"></div>
        <img src={logo} className="logo__img" alt="" />
      </div>
    </div>
  );
};

export default Logo;
