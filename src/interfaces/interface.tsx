export interface IHeroes {
  name: string;
  id: number;
  thumbnail: {
    extension: string;
    path: string;
  };
  description: string;
  total: number;
}

export interface HomePageState {
  loading: boolean;
  inputValue: string;
  heroes: IHeroes[];
  total: number;
}

export interface axiosData {
  data: {
    results: IHeroes[];
    total: number;
  };
}

export interface ComicsState {
  comics: IHeroes[];
  loading: boolean;
  total: number;
}

export interface ComicsPageRouteProps {
  id: string;
}

export interface HomePageProps {
  id: string;
}

export interface HeroesResultProps {
  heroes: IHeroes[] | null;
  loading: boolean;
}

export interface ComicsResultsProps {
  comics: IHeroes[];
  loading: boolean;
}
